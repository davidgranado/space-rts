const MergeIntoSingleFilePlugin = require('webpack-merge-and-include-globally');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const { resolve, join } = require('path');

/**
* @typedef { import('webpack').Configuration } Configuration
*
* @type {Configuration}
*/
module.exports = {
	entry: {
		app: './src/app.js',
	},
	output: {
		path: join(__dirname, 'build'),
		filename: 'bundle.js',
	},
	externals: {
		'phaser': 'Phaser',
	},
	resolve: {
		extensions: [
			'.js',
			'.jsx',
			'.ts',
			'.tsx',
		],
		alias: {
			'@common': resolve(__dirname, './src/common'),
			'@components': resolve(__dirname, './src/components'),
			'@interface': resolve(__dirname, './src/interface'),
			'@prefabs': resolve(__dirname, './src/prefabs'),
			'@resources': resolve(__dirname, './src/resources'),
			'@root': resolve(__dirname, './src/'),
			'@states': resolve(__dirname, './src/states'),
			'@systems': resolve(__dirname, './src/systems'),
		},
	},
	module: {
		rules: [
			{
				test: /\.(tsx?|jsx?)$/,
				use: ['ts-loader'],
				exclude: /node_modules/,
			}, {
				test: /\.css$/i,
				use: [
					'style-loader',
					'css-loader',
				],
			},
		],
	},
	plugins: [
		new MergeIntoSingleFilePlugin({
			files: {
				"vendor.js": [
					//use custom version of phaser built from
					// package's "arcadephysics" grun task
					'libs/phaser-arcade-physics.min.js',
				],
			},
		}),
		new CopyPlugin({
			patterns: [
				{
					from: resolve(__dirname, './src/static'),
				},
			],
		}),
		new HtmlWebpackPlugin({
			template: resolve(__dirname, './src/index.html'),
		}),
	],
	devtool: 'source-map',
	devServer: {
		contentBase: join(__dirname, './build'),
		historyApiFallback: true,
		compress: true,
		port: 9000,
	},
};
