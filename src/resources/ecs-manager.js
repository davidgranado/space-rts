import ECSManager from '@common/ecs-manager';
import instanceManager from '@common/instance-manager';

instanceManager.registerResource('ecs-manager', {
	init() {
		// TODO Remove debug variable
		return window.ecsManager = new ECSManager();
	},
});
